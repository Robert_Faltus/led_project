/*
 Name:		ledStrip.ino
 Created:	11.7.2018 10:12:26
 Author:	JSC-LENOVO
*/

#include <TimerOne.h>
#include <FastLED.h>
#include "LEDdefines.h"

CRGB ledStrip[NUM_LEDS];

uint8_t redColor;
uint8_t greenColor;
uint8_t blueColor;

uint8_t ledCounter = 0;

Color_Typedef selectedColor = LightYellow;

void setup() {
	Serial.begin(115200);

	FastLED.addLeds<LED_TYPE, DATA_PIN, COLOR_ORDER>(ledStrip, NUM_LEDS).setCorrection(TypicalLEDStrip);

	Timer1.initialize(TIME_PERIOD);
	Timer1.attachInterrupt(performProcess);

	setColors(selectedColor);
}

void setColors(Color_Typedef definedColor) {
	switch (definedColor) {
	case LightYellow:
		redColor = 200;
		greenColor = 200;
		blueColor = 38;
		break;
	case WarmYellow:
		redColor = 200;
		greenColor = 200;
		blueColor = 10;
		break;
	case ColdWhite:
		redColor = 200;
		greenColor = 200;
		blueColor = 200;
		break;
	case WarmWhite:
		redColor = 200;
		greenColor = 200;
		blueColor = 55;
		break;
	default:
		break;
	}
}

void fillPartOfStrip() {
	Serial.println(ledCounter);
	fill_gradient_RGB(&ledStrip[ledCounter], 1, CRGB(redColor, greenColor, blueColor), CRGB(0, 0, 0));
	FastLED.delay(1);
}

void performProcess() {
	fillPartOfStrip();

	if (ledCounter == NUM_LEDS) {
		ledCounter = 0;
		setNewCycle(WAITING_FOR_NEW_CYCLE);
	}
	else {
		ledCounter++;
	}
}

void setNewCycle(uint16_t delayTime) {
	Timer1.stop();
	delay(delayTime);
	ledStrip[NUM_LEDS].fadeLightBy(256);
	Timer1.resume();
}

void loop() {

}
