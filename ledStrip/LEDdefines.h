#pragma once

#ifndef LED_STRIP
#define LED_STRIP

#define DATA_PIN    12
#define LED_TYPE    WS2812B
#define COLOR_ORDER GRB
#define NUM_LEDS    60
#define WAITING_FOR_NEW_CYCLE 10000//milliseconds

#define TIME_PERIOD 15000 //microseconds

typedef enum {
	LightYellow,
	WarmYellow,
	ColdWhite,
	WarmWhite
} Color_Typedef;

#endif